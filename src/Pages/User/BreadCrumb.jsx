import React from "react";

export default function BreadCrumb() {
  return (
    <>
      <nav className="bread-crumb" aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <i className="far fa-envelope-open"></i>
          </li>

          <li className="breadcrumb-item active">Benutzerverwaltung</li>

          <li className="breadcrumb-item" aria-current="page">
            <a href="#">Benutzerlogbuch</a>
          </li>
        </ol>
      </nav>
    </>
  );
}
