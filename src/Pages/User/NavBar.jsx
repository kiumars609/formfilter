import React from "react";

export default function NavBar() {
  return (
    <>
      <nav className="navbar navbar-expand-lg py-0 navbar-light">
        <div className="container-fluid">
          <a className="navbar-brand col-1 text-center" href="#">
            <img
              src="./assets/image/logo/logo.png"
              className="col-8"
              alt="logo"
            />
          </a>

          <div className="collapse navbar-collapse p-0" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 p-0">
              <li className="nav-item">
                <span className="nav-link active fw-bold" aria-current="page">
                  Benutzerlogbuch
                </span>
              </li>
            </ul>

            {/* Right Side */}
            <span className="navbar-text d-flex flex-row-reverse user-info py-0">
              {/* User */}
              <div className="col-3 d-flex avatar-box py-0">
                <div className="rounded-circle avatar col-6">
                  <span>
                    <img
                      src="./assets/image/User_Avatar.png"
                      className="col-12"
                    />
                  </span>
                </div>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0 user-info ">
                  <li className="nav-item dropdown ">
                    <a
                      className="nav-link dropdown-toggle"
                      href="#"
                      id="navbarDropdown"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      Tobias Kraus
                    </a>
                  </li>
                </ul>
              </div>

              {/* Notification */}
              <div className="col-1 notif-box text-center">
                <i className="far fa-bell"></i>
              </div>
              {/* Search */}
              <div className="col-1 search-box text-center">
                <i className="fas fa-search"></i>
              </div>
            </span>
          </div>
        </div>
      </nav>

      {/* Bottom Nav */}

      <nav className="navbar navbar-bottom navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Dashboard
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Übersicht
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Anlagenzustand
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Analyse
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Einstellungen
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Alarm-Manager
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Benutzerverwaltung
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  Administration
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
