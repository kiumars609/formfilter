import React, { useState } from "react";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css"; // optional
import Filters from "./Filters/";

export default function LeftNav({
  data,
  handleMandantFilter,
  handleBenutzerFilter,
  handleKategorienFilter,
  handleObjecktFilter,
  handleDatumFirstFilter,
  handleDatumSecondFilter,
}) {
  const [show, setShow] = useState(false);
  const handleClose = (e) => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <div className="main-icons text-center mh-100">
        <div className="main-filter col-12" onClick={handleShow}>
          <Tippy content="Filter" interactive={true} interactiveBorder={20}>
            <i className="fas fa-filter"></i>
          </Tippy>
        </div>

        <div className="main-filter mt-4">
          <Tippy content="Drucken" interactive={true} interactiveBorder={20}>
            <i className="fa fa-print"></i>
          </Tippy>
        </div>

        <div className="main-filter mt-4">
          <Tippy
            content="Exportieren"
            interactive={true}
            interactiveBorder={20}
          >
            <i className="fas fa-file-export"></i>
          </Tippy>
        </div>

        <Filters
          handleMandantFilter={handleMandantFilter}
          handleBenutzerFilter={handleBenutzerFilter}
          handleKategorienFilter={handleKategorienFilter}
          handleObjecktFilter={handleObjecktFilter}
          handleDatumFirstFilter={handleDatumFirstFilter}
          handleDatumSecondFilter={handleDatumSecondFilter}
          data={data}
          showModal={show}
          hideModal={handleClose}
        />
      </div>
    </>
  );
}
