import React, { useEffect, useState } from "react";
import "./scss/style.css";
import NavBar from "./NavBar";
import BreadCrumb from "./BreadCrumb";
import Results from "./Results/";
import { get } from "../../services";
import LeftNav from "./LeftNav";

export default function UserAction() {
  const [data, setData] = useState([]);
  const [filterMandant, setFilterMandant] = useState([]);
  const [filterBenutzer, setFilterBenutzer] = useState([]);
  const [filterKategorien, setFilterKategorien] = useState([]);
  const [filterObjeckt, setFilterObjeckt] = useState([]);
  const [filterDatumFirst, setfilterDatumFirst] = useState([]);
  const [filterDatumSecond, setfilterDatumSecond] = useState([]);

  useEffect(() => {
    get("https://mr-kiumars.ir/formfilter/api/user/userAPI.php").then(
      (response) => {
        setData(response);
      }
    );
  }, []);

  // start get value from filter options
  const handleMandantFilter = (e) => {
    // When CheckBox Clicked
    if (e.target.checked) {
      setFilterMandant([...filterMandant, e.target.value]);
    }
    // When CheckBox Not Clicked
    else {
      // Hier First Find Index of Value That Clicked To Disable
      const index = filterMandant.findIndex((obj) => obj === e.target.value);
      // After Find Index of Value, Delete That Value Without Another Filter Clicked
      const newData = [
        ...filterMandant.slice(0, index),
        ...filterMandant.slice(index + 1),
      ];
      setFilterMandant(newData);
    }
  };

  const handleBenutzerFilter = (e) => {
    // When CheckBox Clicked
    if (e.target.checked) {
      setFilterBenutzer([...filterBenutzer, e.target.value]);
    }
    // When CheckBox Not Clicked
    else {
      // Hier First Find Index of Value That Clicked To Disable
      const index = filterBenutzer.findIndex((obj) => obj === e.target.value);
      // After Find Index of Value, Delete That Value Without Another Filter Clicked
      const newData = [
        ...filterBenutzer.slice(0, index),
        ...filterBenutzer.slice(index + 1),
      ];
      setFilterBenutzer(newData);
    }
  };

  const handleKategorienFilter = (e) => {
    // When CheckBox Clicked
    if (e.target.checked) {
      setFilterKategorien([...filterKategorien, e.target.value]);
    }
    // When CheckBox Not Clicked
    else {
      // Hier First Find Index of Value That Clicked To Disable
      const index = filterKategorien.findIndex((obj) => obj === e.target.value);
      // After Find Index of Value, Delete That Value Without Another Filter Clicked
      const newData = [
        ...filterKategorien.slice(0, index),
        ...filterKategorien.slice(index + 1),
      ];
      setFilterKategorien(newData);
    }
  };

  const handleObjecktFilter = (e) => {
    // When CheckBox Clicked
    if (e.target.checked) {
      setFilterObjeckt([...filterObjeckt, e.target.value]);
    }
    // When CheckBox Not Clicked
    else {
      // Hier First Find Index of Value That Clicked To Disable
      const index = filterObjeckt.findIndex((obj) => obj === e.target.value);
      // After Find Index of Value, Delete That Value Without Another Filter Clicked
      const newData = [
        ...filterObjeckt.slice(0, index),
        ...filterObjeckt.slice(index + 1),
      ];
      setFilterObjeckt(newData);
    }
  };

  const handleDatumFirstFilter = (entire) => {
    // When CheckBox Clicked
    if (entire) {
      setfilterDatumFirst(entire);
    }
  };

  const handleDatumSecondFilter = (entire) => {
    if (entire) {
      setfilterDatumSecond(entire);
    }
  };
  // end get value from filter options

  return (
    <>
      <NavBar />
      <div className="d-flex">
        <div className="col-lg-1 col-sm-4 left-nav min-vh-100 bg-dark">
          <LeftNav
            handleMandantFilter={handleMandantFilter}
            handleBenutzerFilter={handleBenutzerFilter}
            handleKategorienFilter={handleKategorienFilter}
            handleObjecktFilter={handleObjecktFilter}
            handleDatumFirstFilter={handleDatumFirstFilter}
            handleDatumSecondFilter={handleDatumSecondFilter}
            data={data}
          />
        </div>
        <div className="col-11">
          <BreadCrumb />
          <h5 className="page-title">Benutzerlogbuch</h5>
          <Results
            data={data}
            filterMandant={filterMandant}
            filterBenutzer={filterBenutzer}
            filterKategorien={filterKategorien}
            filterObjeckt={filterObjeckt}
            filterDatumFirst={filterDatumFirst}
            filterDatumSecond={filterDatumSecond}
          />
        </div>
      </div>
      <div className="col-sm-12 navbar-bottom-mobile">
        <LeftNav
          handleMandantFilter={handleMandantFilter}
          handleBenutzerFilter={handleBenutzerFilter}
          handleKategorienFilter={handleKategorienFilter}
          handleObjecktFilter={handleObjecktFilter}
          handleDatumFirstFilter={handleDatumFirstFilter}
          handleDatumSecondFilter={handleDatumSecondFilter}
          data={data}
        />
      </div>
    </>
  );
}
