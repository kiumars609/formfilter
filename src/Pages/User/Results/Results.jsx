import React from "react";
import DataTable from "react-data-table-component";
import { Table } from "./Table";
import Loading from "../../../Component/Loading/";

export default function Results({
  data,
  filterMandant,
  filterBenutzer,
  filterKategorien,
  filterObjeckt,
  filterDatumFirst,
  filterDatumSecond,
}) {
  let newData = [...data];

  if (filterMandant.length !== 0) {
    newData = newData.filter(
      (filter) => filter.mandant && filterMandant.includes(filter.mandant)
    );
  }
  if (filterBenutzer.length !== 0) {
    newData = newData.filter(
      (filter) => filter.benutzer && filterBenutzer.includes(filter.benutzer)
    );
  }
  if (filterKategorien.length !== 0) {
    newData = newData.filter(
      (filter) =>
        filter.kategorien && filterKategorien.includes(filter.kategorien)
    );
  }
  if (filterObjeckt.length !== 0) {
    newData = newData.filter(
      (filter) => filter.objeckt && filterObjeckt.includes(filter.objeckt)
    );
  }
  if (filterDatumFirst.length !== 0 && filterDatumSecond.length !== 0) {
    newData = newData.filter(
      (filter) =>
        filter.createdAt &&
        String(filterDatumFirst) <=
          filter.createdAt.slice(0, 10).replaceAll("-", "/") &&
        String(filterDatumSecond) >=
          filter.createdAt.slice(0, 10).replaceAll("-", "/")
    );
  }

  const tbl = Table(newData);

  return (
    <>
      <div className="container-fluid main-table-result p-3">
        {/* <h5>Ergebnisse</h5> */}
        <DataTable
          title="Ergebnisse"
          striped="true"
          customStyles={tbl.customStyles}
          columns={tbl.columns}
          data={tbl.filteredItems}
          expandableRowsComponent={tbl.ExpandedComponent}
          pagination
          highlightOnHover="true"
          progressPending={tbl.pending}
          paginationResetDefaultPage={tbl.resetPaginationToggle}
          subHeaderComponent={tbl.subHeaderComponentMemo}
          persistTableHead
          progressComponent={<Loading />}
        />
      </div>
    </>
  );
}
