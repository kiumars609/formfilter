import React, { useEffect } from "react";
import { createTheme } from "react-data-table-component";
import styled from "styled-components";

export function Table(data) {
  // createTheme creates a new theme named solarized that overrides the build in dark theme
  createTheme(
    "solarized",
    {
      text: {
        primary: "#268bd2",
        secondary: "#2aa198",
      },
      background: {
        default: "#002b36",
      },
      context: {
        background: "#cb4b16",
        text: "#FFFFFF",
      },
      divider: {
        default: "#073642",
      },
      action: {
        button: "rgba(0,0,0,.54)",
        hover: "rgba(0,0,0,.08)",
        disabled: "rgba(0,0,0,.12)",
      },
    },
    "dark"
  );

  createTheme("dark", {
    background: {
      default: "transparent",
    },
  });

  const columns = [
    {
      name: "Nr",
      selector: (row) => row.id,
      sortable: true,
      reorder: true,
      style: {
        width: "100px",
        // background: 'red'
      },
    },
    {
      name: "Datum",
      selector: (row) => row.createdAt,
      sortable: true,
      reorder: true,
    },
    {
      name: "Kategorien",
      selector: (row) => row.kategorien,
      sortable: true,
    },
    {
      name: "Mandant",
      selector: (row) => row.mandant,
      sortable: true,
    },
    {
      name: "Benutzer",
      selector: (row) => row.benutzer,
      sortable: true,
    },
    {
      name: "Objeckt",
      selector: (row) => row.objeckt,
      sortable: true,
    },
    {
      name: "Aktionen",
      selector: (row) => row.aktionen,
      sortable: true,
    },
  ];

  const ExpandedComponent = ({ data }) => (
    <pre>{JSON.stringify(data, null, 2)}</pre>
  );

  const customStyles = {
    headCells: {
      style: {
        background: "#d7dce1",
      },
    },
  };

  const [pending, setPending] = React.useState(true);
  const [rows, setRows] = React.useState([]);
  const [filterText, setFilterText] = React.useState("");
  const [resetPaginationToggle, setResetPaginationToggle] = React.useState(
    false
  );

  useEffect(() => {
    const timeout = setTimeout(() => {
      setRows(data);
      setPending(false);
    }, 2000);
    return () => clearTimeout(timeout);
  }, []);

  const TextField = styled.input`
    height: 32px;
    width: 200px;
    border-radius: 3px;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border: 1px solid #e5e5e5;
    padding: 0 32px 0 16px;

    &:hover {
      cursor: pointer;
    }
  `;

  const FilterComponent = ({ filterText, onFilter, onClear }) => (
    <>
      <TextField
        id="search"
        type="text"
        placeholder="nach Namen filtern"
        aria-label="Search Input"
        value={filterText}
        onChange={onFilter}
      />
    </>
  );

  const filteredItems = data.filter(
    (item) =>
      item.benutzer &&
      item.benutzer.toLowerCase().includes(filterText.toLowerCase())
  );

  const subHeaderComponentMemo = React.useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText("");
      }
    };
    return (
      <FilterComponent
        onFilter={(e) => setFilterText(e.target.value)}
        onClear={handleClear}
        // filterText={filterText}
      />
    );
  }, [resetPaginationToggle]);
  return {
    customStyles,
    columns,
    filteredItems,
    ExpandedComponent,
    pending,
    resetPaginationToggle,
    subHeaderComponentMemo,
  };
}
