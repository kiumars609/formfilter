import React from "react";
import Modal from "react-bootstrap/Modal";

export default function DataModal({ children, showModal, hideModal }) {
  return (
    <>
      <Modal
        className="modal-right modal-card filter"
        size="sm"
        show={showModal}
        onHide={hideModal}
      >
        <Modal.Header>
          {/* <Modal.Title>Modal heading</Modal.Title> */}
          <span>Filters</span>
        </Modal.Header>
        <Modal.Body>{children}</Modal.Body>
        <Modal.Footer>
          <button className="btn btn-dark" onClick={hideModal} closeButton>
            Close Filter Panel
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
