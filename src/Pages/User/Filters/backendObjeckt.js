import React from "react";

export function backendObjeckt(data, handleObjecktFilter) {
  const uniq = Object.values(
    data.reduce((r, c) => {
      r[c.objeckt] = c;
      return r;
    }, {})
  );

  const postList =
    uniq.length &&
    uniq.map((item) => {
      return (
        <li key={item.id} className="new-check-checkbox">
          <input
            type="checkbox"
            id={item.objeckt}
            onChange={(e) => handleObjecktFilter(e)}
            className="checkbox"
            value={item.objeckt}
          />
          <label for={item.objeckt}>{item.objeckt}</label>
        </li>
      );
    });

  return postList;
}
