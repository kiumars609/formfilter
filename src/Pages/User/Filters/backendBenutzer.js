import React from "react";

export function backendBenutzer(data, handleMandantFilter) {
  const uniq = Object.values(
    data.reduce((r, c) => {
      r[c.benutzer] = c;
      return r;
    }, {})
  );

  const postList =
    uniq.length &&
    uniq.map((item) => {
      return (
        <div key={item.id} class="form-check form-switch mb-3">
          <input
            class="form-check-input"
            type="checkbox"
            id={item.benutzer}
            onChange={(e) => handleMandantFilter(e)}
            value={item.benutzer}
          />
          <label class="form-check-label" for={item.benutzer}>
            {item.benutzer}
          </label>
        </div>
      );
    });

  return postList;
}
