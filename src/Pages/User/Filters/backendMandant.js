import React from "react";

export function backendMandant(data, handleMandantFilter) {
  const uniq = Object.values(
    data.reduce((r, c) => {
      r[c.mandant] = c;
      return r;
    }, {})
  );

  const postList =
    uniq.length &&
    uniq.map((item) => {
      return (
        <div key={item.id} class="form-check form-switch mb-3">
          <input
            class="form-check-input"
            type="checkbox"
            id={item.mandant}
            onChange={(e) => handleMandantFilter(e)}
            value={item.mandant}
          />
          <label class="form-check-label" for={item.mandant}>
            {item.mandant}
          </label>
        </div>
      );
    });

  return postList;
}
