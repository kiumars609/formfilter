import React from "react";
import DataModal from "./DataModal";
import { backendMandant } from "./backendMandant";
import { backendBenutzer } from "./backendBenutzer";
import { backendKategorien } from "./backendKategorien";
import { backendObjeckt } from "./backendObjeckt";
import { backendDatum } from "./backendDatum";

export default function Filters({
  showModal,
  hideModal,
  handleMandantFilter,
  handleBenutzerFilter,
  handleKategorienFilter,
  handleObjecktFilter,
  handleDatumFirstFilter,
  handleDatumSecondFilter,
  data,
}) {
  const queryMandant = backendMandant(data, handleMandantFilter);
  const queryBenutzer = backendBenutzer(data, handleBenutzerFilter);
  const queryKategorien = backendKategorien(data, handleKategorienFilter);
  const queryObjeckt = backendObjeckt(data, handleObjecktFilter);
  const queryDatum = backendDatum(
    handleDatumFirstFilter,
    handleDatumSecondFilter
  );

  return (
    <>
      <DataModal showModal={showModal} hideModal={hideModal}>
        <div className="accordion accordion-flush" id="accordionFlushExample">
          {/* Start Mandant Filter */}
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingOne">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseOne"
                aria-expanded="false"
                aria-controls="flush-collapseOne"
              >
                Mandant
              </button>
            </h2>
            <div
              id="flush-collapseOne"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingOne"
              data-bs-parent="#accordionFlushExample"
            >
              <div className="accordion-body">
                {queryMandant}
              </div>
            </div>
          </div>
          {/* End Mandant Filter */}

          {/* Start Benutzer Filter */}
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingTwo">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseTwo"
                aria-expanded="false"
                aria-controls="flush-collapseTwo"
              >
                Benutzer
              </button>
            </h2>
            <div
              id="flush-collapseTwo"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingTwo"
              data-bs-parent="#accordionFlushExample"
            >
              <div className="accordion-body">
                {queryBenutzer}
              </div>
            </div>
          </div>
          {/* End Benutzer Filter */}

          {/* Start Kategorien Filter */}
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingThree">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseThree"
                aria-expanded="false"
                aria-controls="flush-collapseThree"
              >
                Kategorien
              </button>
            </h2>
            <div
              id="flush-collapseThree"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingThree"
              data-bs-parent="#accordionFlushExample"
            >
              <div className="accordion-body">
                <ul className="ks-cboxtags p-0">{queryKategorien}</ul>
              </div>
            </div>
          </div>
          {/* End Kategorien Filter */}

          {/* Start Objeckt Filter */}
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingFour">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseFour"
                aria-expanded="false"
                aria-controls="flush-collapseFour"
              >
                Objeckt
              </button>
            </h2>
            <div
              id="flush-collapseFour"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingFour"
              data-bs-parent="#accordionFlushExample"
            >
              <div className="accordion-body">{queryObjeckt}</div>
            </div>
          </div>
          {/* End Objeckt Filter */}

          {/* Start Datum Filter */}
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingFive">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseFive"
                aria-expanded="false"
                aria-controls="flush-collapseFive"
              >
                Datum
              </button>
            </h2>
            <div
              id="flush-collapseFive"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingFive"
              data-bs-parent="#accordionFlushExample"
            >
              <div className="accordion-body">{queryDatum}</div>
            </div>
          </div>
          {/* End Datum Filter */}
        </div>
      </DataModal>
    </>
  );
}
