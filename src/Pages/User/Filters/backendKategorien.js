import React from "react";

export function backendKategorien(data, handleKategorienFilter) {
  const uniq = Object.values(
    data.reduce((r, c) => {
      r[c.kategorien] = c;
      return r;
    }, {})
  );

  const postList =
    uniq.length &&
    uniq.map((item) => {
      return (
        <li key={item.id}>
          <input
            type="checkbox"
            id={item.kategorien}
            onChange={(e) => handleKategorienFilter(e)}
            value={item.kategorien}
          />
          <label for={item.kategorien}>{item.kategorien}</label>
        </li>
      );
    });

  return postList;
}
