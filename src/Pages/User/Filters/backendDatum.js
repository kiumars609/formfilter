import React, { forwardRef, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export function backendDatum(handleDatumFirstFilter, handleDatumSecondFilter) {
  const [startDate, setStartDate] = useState([]);
  const [endDate, setEndDate] = useState([]);

  handleDatumFirstFilter(startDate);
  handleDatumSecondFilter(endDate);

  const options = {
    month: "2-digit",
    year: "numeric",
    day: "2-digit",
  };

  const StartCustomInput = forwardRef(({ value, onClick }, ref) => (
    <div class="input-group mb-3 select-datepicker-style">
      <i className="fas fa-calendar-alt input-group-text" id="basic-addon1"></i>
      <input
        type="text"
        class="form-control"
        placeholder="Start Date"
        value={value}
        onClick={onClick}
        ref={ref}
        aria-label="Start Date"
        aria-describedby="basic-addon1"
      />
    </div>
  ));

  const EndCustomInput = forwardRef(({ value, onClick }, ref) => (
    <div class="input-group mb-3 select-datepicker-style">
      <i className="fas fa-calendar-alt input-group-text" id="basic-addon2"></i>
      <input
        type="text"
        class="form-control"
        placeholder="End Date"
        value={value}
        onClick={onClick}
        ref={ref}
        aria-label="End Date"
        aria-describedby="basic-addon2"
      />
    </div>
  ));

  return (
    <>
      <DatePicker
        value={startDate}
        onChange={(date) => {
          const start = new Date(date).toLocaleDateString(
            "zh-Hans-CN",
            options
          );
          setStartDate(start);
        }}
        selectsStart
        startDate={startDate}
        placeholderText="Start Date"
        customInput={<StartCustomInput />}
      />
      <DatePicker
        value={endDate}
        onChange={(date) => {
          const end = new Date(date).toLocaleDateString("zh-Hans-CN", options);
          setEndDate(end);
        }}
        selectsEnd
        endDate={endDate}
        placeholderText="End Date"
        customInput={<EndCustomInput />}
      />
    </>
  );
}
