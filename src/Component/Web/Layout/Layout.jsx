import React,{useEffect} from "react";
import Footer from "./Footer/Footer";
import NavBar from "./NavBar/NavBar";
import "./scss/layout.css";

export default function Layout({ children, pageTitle }) {
  useEffect(() => {
    document.title = "AutoRoad724 | " + pageTitle;
  }, [pageTitle]);

  return (
    <>
      <NavBar />
      {children}
      <Footer />

      {/* <!-- loader --> */}
      <div id="ftco-loader" className="show fullscreen">
        <svg className="circular" width="48px" height="48px">
          <circle
            className="path-bg"
            cx="24"
            cy="24"
            r="22"
            fill="none"
            stroke-width="4"
            stroke="#eeeeee"
          />
          <circle
            className="path"
            cx="24"
            cy="24"
            r="22"
            fill="none"
            stroke-width="4"
            stroke-miterlimit="10"
            stroke="#F96D00"
          />
        </svg>
      </div>
    </>
  );
}
